package Week3;

import java.util.Scanner;

public class DataValidation {
    public DataValidation() {
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int quotient;
        System.out.println("Enter number 1");
        int num1 = input.nextInt();
        System.out.println("Enter Number 2");
        int num2 = input.nextInt();
        for(quotient = divide(num1, num2); num2 == 0; num2 = input.nextInt()) {
            System.out.println("Please enter a number that is not 0 for the second number.");
        }

        System.out.println("The quotient is " + quotient);
    }

    public static int divide(int num1, int num2) throws ArithmeticException {
        int quotient = num1 / num2;
        return quotient;
    }
}