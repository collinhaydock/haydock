package Week4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HTTP {
    public HTTP() {
    }

    public static String getHttpContent(String string) {
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;

            while((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();
        } catch (IOException var6) {
            System.err.println(var6.toString());
            return "Error";
        }
    }

    public static Map getHttpHeaders(String string) {
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            return http.getHeaderFields();
        } catch (IOException var3) {
            System.out.println(var3.toString());
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(getHttpContent("http://www.google.com"));
        Map<Integer, List<String>> m = getHttpHeaders("http://www.google.com");
        Iterator var2 = m.entrySet().iterator();

        while(var2.hasNext()) {
            Entry<Integer, List<String>> entry = (Entry)var2.next();
            PrintStream var10000 = System.out;
            Object var10001 = entry.getKey();
            var10000.println("Key = " + var10001 + " Value = " + entry.getValue());
        }

    }
}
