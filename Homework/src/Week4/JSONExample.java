package Week4;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONExample {
    public JSONExample() {
    }

    public static String wowClassToJSON(WoWClass wowClass) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(wowClass);
        } catch (JsonProcessingException var4) {
            System.err.println(var4.toString());
        }

        return s;
    }

    public static WoWClass JSONToWowClass(String s) {
        ObjectMapper mapper = new ObjectMapper();
        WoWClass wClass = null;

        try {
            wClass = (WoWClass)mapper.readValue(s, WoWClass.class);
        } catch (JsonProcessingException var4) {
            System.err.println(var4.toString());
        }

        return wClass;
    }

    public static void main(String[] args) {
        WoWClass wClass1 = new WoWClass("Warrior", "Fury");
        String json = wowClassToJSON(wClass1);
        System.out.println(json);
        WoWClass wClass2 = JSONToWowClass(json);
        System.out.println(wClass2);
    }
}
