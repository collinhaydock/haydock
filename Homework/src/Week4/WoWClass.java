package Week4;
import java.util.Set;
import java.util.TreeSet;

public class WoWClass {
    private String wowClass;
    private String spec;

    public WoWClass(String wowClass, String spec) {
        this.wowClass = wowClass;
        this.spec = spec;
    }


    public String getWowClass() {

        return wowClass;
    }

    public void setWowClass(String wowClass) {

        this.wowClass = wowClass;
    }

    public String getSpec() {

        return spec;
    }

    public void setSpec(String spec) {

        this.spec = spec;
    }



    public String toString() {
        return "Class: " + this.wowClass + ", Spec: " + this.spec;
    }

    public static void main(String[] args) {


        WoWClass Fury = new WoWClass("Warrior", "Fury");
        WoWClass Arms = new WoWClass("Warrior", "Arms");
        WoWClass Protection = new WoWClass("Warrior", "Protection");
        Set warrior = new TreeSet();
        warrior.add(Fury);
        warrior.add(Arms);
        warrior.add(Protection);

        WoWClass Affliction = new WoWClass("Warlock", "Affliction");
        WoWClass Demonology = new WoWClass("Warlock", "Demonology");
        WoWClass Destruction = new WoWClass("Warlock", "Destruction");
            Set warlock = new TreeSet();
        warlock.add(Affliction);
        warlock.add(Demonology);
        warlock.add(Destruction);

        WoWClass Holy = new WoWClass("Paladin", "Holy");
        WoWClass Prot = new WoWClass("Paladin", "Protection");
        WoWClass Retribution = new WoWClass("Paladin", "Retribution");

        Set paladin = new TreeSet();
        paladin.add(Holy);
        paladin.add(Prot);
        paladin.add(Retribution);

        WoWClass Marksman = new WoWClass("Hunter", "Marksman");
        WoWClass Survival = new WoWClass("Hunter", "Survival");
        WoWClass BeastMastery = new WoWClass("Hunter", "Beast Mastery");
        Set hunter = new TreeSet();
        hunter.add(Marksman);
        hunter.add(Survival);
        hunter.add(BeastMastery);

        WoWClass Frost = new WoWClass("Mage", "Frost");
        WoWClass Fire = new WoWClass("Mage", "Fire");
        WoWClass Arcane = new WoWClass("Mage", "Arcane");
        Set mage = new TreeSet();
        mage.add(Frost);
        mage.add(Fire);
        mage.add(Arcane);

        WoWClass Subtlety = new WoWClass("Rogue", "Subtlety");
        WoWClass Assassination = new WoWClass("Rogue", "Assassination");
        WoWClass Combat = new WoWClass("Rogue", "Combat");
        Set rogue = new TreeSet();
        rogue.add(Subtlety);
        rogue.add(Assassination);
        rogue.add(Combat);

        WoWClass Feral = new WoWClass("Druid", "Feral");
        WoWClass Restoration = new WoWClass("Druid", "Restoration");
        WoWClass Balance = new WoWClass("Druid", "Balance");
        Set druid = new TreeSet();
        druid.add(Feral);
        druid.add(Restoration);
        druid.add(Balance);

        WoWClass Enhancement = new WoWClass("Shaman", "Enhancement");
        WoWClass Elemental = new WoWClass("Shaman", "Elemental");
        WoWClass Resto = new WoWClass("Shaman", "Restoration");
        Set shaman = new TreeSet();
        shaman.add(Enhancement);
        shaman.add(Elemental);
        shaman.add(Resto);

        WoWClass Discipline = new WoWClass("Priest", "Discipline");
        WoWClass Shadow = new WoWClass("Priest", "Shadow");
        WoWClass HolyPriest = new WoWClass("Priest", "Holy");
        Set priest = new TreeSet();
        priest.add(Discipline);
        priest.add(Shadow);
        priest.add(HolyPriest);

    }

}
