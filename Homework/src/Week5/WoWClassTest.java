package Week5;

import Week4.WoWClass;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class WoWClassTest {

    @Test
    void getWowClass() {
            WoWClass class9 = new WoWClass("Mage", "Arcane");
            assertEquals("Mage", class9.getWowClass());
    }

    @Test
    void getSpec () {
        WoWClass class9 = new WoWClass("Mage", "Arcane");
        assertEquals("Arcane", class9.getSpec());
    }
    @Test
    void setWowClass() {
        WoWClass class2 = new WoWClass("Mage", null);
        class2.setWowClass("Warrior");
        assertEquals("Warrior", class2.getWowClass());
        assertNull(class2.getSpec());
    }

    @Test
    void setSpec () {
        WoWClass class2 = new WoWClass(null, "Arcane");
        class2.setSpec("Fury");
        assertEquals("Fury", class2.getSpec());
        assertNotSame("Warrior", class2.getSpec());
        assertNull(class2.getWowClass());
    }

    @Test
    void testToString() {
        WoWClass class2 = new WoWClass("Warrior", "Fury");
        WoWClass class1 = new WoWClass("Mage", "Fire");
        WoWClass class3 = new WoWClass("", "");
        assertNotNull(class2);
        assertNotSame(class2, class1);
        assertSame(class1, class1);

    }

    @Test
    void main() {
        Object[] class2 = {"Warrior","Fury"};
        Object[] class3 = {"Warrior", "Fury"};
        Object[] class4 = {"Mage", "Fire"};
        assertArrayEquals(class3, class2);

    }

}